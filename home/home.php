<!DOCTYPE html>
<html lang="en">

<?php include "../templates/header.php"; ?>

<body>

<div class="main">
    <h2>Latest articles</h2>
    <div class="centerbox">
        <h1><a href="../pages/editoriale%20major.php" class="black">Ce inseamna un Major</a></h1>
        <p>Dota 2 este precum un uragan: într-o continuă crestere şi dezvoltare. Deşi alte sporturi electronice vin şi pleacă, opera celor de la Valve se modelează, evoluează şi reprezintă un pilon de susţinere  pentru viitorul esports..</p>
        <div class="img">
            <img src="../images/PGLgroup.jpg" alt="PGL group" width="800">
        </div>
    </div>

    <hr>

    <div class="centerbox">
        <h1><a href="../pages/review%20asus.php" class="black">Review Router Asus RT-N66U</a></h1>
        <p>Transmisie Dual-Band de 2,4 GHz și 5 GHz pentru putere mare de semnal și conexiunea ultra-rapidă de până la 900Mbps, cel mai bun router pentru acasă și pentru la muncă.
            Te joci mult sau petreci mult timp făcând streaming video? Indiferent de plan, RT-N66U deține tot ce ai nevoie..</p>
        <div class="img">
            <img src="../images/router_asus_rtn66u.png" alt="Router asus">
        </div><br>
    </div>

    <hr>

    <div class="centerbox">
        <h1><a href="../pages/noutati%20asus%20rog.php" class="black">Asus Republic of Gamers anunta Strix GL12CX</a></h1>
        <p>ASUS Republic of Gamers (ROG) anunță Strix GL12CX, un desktop de gaming supratactat din fabrică, ce dispune de cel mai nou procesor Intel Core i9-9900K, cu 8 nuclee, din generația a noua și grafica NVIDIA GeForce RTX. Procesorul Intel Core din generația a noua este deblocat pentru ajustări precise cu ajutorul software-ului ROG Armoury Crate, ce permite printre altele și personalizarea iluminatului Aura Sync din interior și exterior. În cadrul sertarului SSD hot-swap gândit pentru competițiile de gaming există un modul DIMM.2 care poate conecta două SSD-uri răcite prin radiatoare dedicate..</p>
        <div class="img">
            <img src="../images/rog_strix_gl12cx_3d_front.jpg" alt="Rog strix gl12cx" width="520">
        </div><br>
    </div>

</div>

<div class="footer">

    <?php include "../templates/footer.php"; ?>

</div>

</body>
</html>
