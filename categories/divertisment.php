<!DOCTYPE html>
<html lang="en">

<?php include "../templates/header.php"; ?>

<body>

<div class="main">
    <div class="centerbox">
        <h1><a href="../pages/divertisment%20bucharest%20gaming%20week.php" class="black">Cum a fost la Bucharest Gaming Week</a></h1>
        <p><b>Bucharest Gaming Week</b> a strâns la prima ediție peste 10.000 de pasionați de esports, gameri dedicați și oameni din industria de game development din România, majoritatea participanților fiind prezentă în weekendul 27-28 ianuarie. Vizitatorii au participat la numeroase activități care s-au desfășurat la standurile expozanților unde au avut loc peste 50 de competiții și tombole, iar fani de toate vârstele s-au adunat pentru a se întâlni cu creatorii de conținut, jucătorii profesioniști și youtuberii preferați..</p>
        <div class="img">
            <img src="../images/BGW_4.jpg" alt="VR" width="800">
        </div>
    </div>
    <hr>
    <div class="centerbox">
        <h1><a href="../pages/divertisment%20gaming.php" class="black">Gaming-ul are public mai mare decât HBO, Hulu, Neftlix și ESPN la un loc</a></h1>
        <p>Așa arată un nou raport al SuperData Research, ce conclude că ne place gaming-ul foarte mult. Conform SuperData, întreaga audiență dedicată gaming-ului include peste 665 de milioane de oameni și crește de la zi la zi: compania prezice o creștere de 21% până în 2021. Aceștia susțin că audiența pentru industria de gaming depășește audiența totală a HBO, Netflix, ESPN și Hulu..</p>
        <div class="img">
            <img src="../images/1_26BIgJm5GZCdYO31v2B1lg.png" alt="Superdata" width="700">
        </div>
        <br>
    </div>
    <hr>
</div>

<div class="footer">

    <?php include "../templates/footer.php"; ?>

</div>

</body>
</html>
