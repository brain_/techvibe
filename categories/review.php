<!DOCTYPE html>
<html lang="en">

<?php include "../templates/header.php"; ?>

<body>

<div class="main">
    <div class="centerbox">
        <h1><a href="../pages/review%20asus.php" class="black">Review Router Asus RT-N66U</a></h1>
        <p>Transmisie Dual-Band de 2,4 GHz și 5 GHz pentru putere mare de semnal și conexiunea ultra-rapidă de până la 900Mbps, cel mai bun router pentru acasă și pentru la muncă.
            Te joci mult sau petreci mult timp făcând streaming video? Indiferent de plan, RT-N66U deține tot ce ai nevoie..</p>
        <div class="img">
            <img src="../images/router_asus_rtn66u.png" alt="Router asus">
        </div><br>
    </div>

    <hr>

    <div class="centerbox">
        <h1><a href="../pages/review%20tastatura.php" class="black">Review Tastatură Logitech G105</a></h1>
        <p>Logitech Gaming Keyboard G105 este o tastatură pe membrană silențioasă care ofera foarte multe optiuni. În ciuda faptului că aceasta nu este o tastatură mecanică, aceasta
            oferă o functionalitate pentru orice tip de utilizator, design atractiv si un pret relativ accesibil..</p><br><br><br>
        <div class="img">
            <img src="../images/logitech_g105.png" alt="Logitech G105" width="700">
        </div>
    </div>

    <hr>

</div>

<div class="footer">

    <?php include "../templates/footer.php"; ?>

</div>

</body>
</html>
