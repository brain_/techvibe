<!DOCTYPE html>
<html lang="en">

<?php include "../templates/header.php"; ?>

<body>

<div class="main">
    <div class="centerbox">
        <h1><a href="../pages/noutati%20asus%20geforce.php" class="black">ASUS Republic of Gamers lansează placa video ROG Strix GeForce RTX 2080 Ti OC Call of Duty: Black Ops 4 Edition</a></h1>
        <p>ASUS Republic of Gamers (ROG) a lansat placa video ROG Strix GeForce RTX 2080 Ti OC Call of Duty: Black Ops 4 Edition. Această ediție va fi disponibilă în cantități limitate, fiind restricționată la doar 500 de unități disponibile numai în anumite zone geografice (inclusiv în România). Decorate cu insigna Call of Duty: Black Ops 4 Edition, plăcile împerechează nuclee NVIDIA din lista A cu performanțe de vârf și o răcire extremă. Acestea au frecvențe de până la 1665MHz, performanțele rămânând constante în timpul acțiunilor intensive..</p>
        <div class="img">
            <img src="../images/ROG-Strix-GeForce-RTX-2080-Ti-OC-3.jpg" alt="ROG Strix geforce" width="800">
        </div>
    </div>
    <hr>
    <div class="centerbox">
        <h1><a href="../pages/noutati%20asus%20rog%20strix.php" class="black">ASUS Republic of Gamers anunță Strix GL12CX</a></h1>
        <p>ASUS Republic of Gamers (ROG) anunță Strix GL12CX, un desktop de gaming supratactat din fabrică, ce dispune de cel mai nou procesor Intel Core i9-9900K, cu 8 nuclee, din generația a noua și grafica NVIDIA GeForce RTX. Procesorul Intel Core din generația a noua este deblocat pentru ajustări precise cu ajutorul software-ului ROG Armoury Crate, ce permite printre altele și personalizarea iluminatului Aura Sync din interior și exterior. În cadrul sertarului SSD hot-swap gândit pentru competițiile de gaming există un modul DIMM.2 care poate conecta două SSD-uri răcite prin radiatoare dedicate..</p>
        <div class="img">
            <img src="../images/rog_strix_gl12cx_3d_front.jpg" alt="CRT LED" width="700">
        </div>
        <br>
    </div>
    <hr>
</div>

<div class="footer">

    <?php include "../templates/footer.php"; ?>

</div>

</body>
</html>
