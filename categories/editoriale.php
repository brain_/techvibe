<!DOCTYPE html>
<html lang="en">

<?php include "../templates/header.php"; ?>

<body>

<div class="main">
    <div class="centerbox">
        <h1><a href="../pages/editoriale%20major.php" class="black">Ce inseamna un Major</a></h1>
        <p>Dota 2 este precum un uragan: într-o continuă crestere şi dezvoltare. Deşi alte sporturi electronice vin şi pleacă, opera celor de la Valve se modelează, evoluează şi reprezintă un pilon de susţinere  pentru viitorul esports..</p>
        <div class="img">
            <img src="../images/PGLgroup.jpg" alt="PGL group" width="800">
        </div>
    </div>
    <hr>
    <div class="centerbox">
        <h1><a href="../pages/editoriale%20crt.php" class="black">De la CRT la LED: O călătorie prin timp</a></h1>
        <p>De la CRT la LED: Industria display-urilor a avansat într-un ritm fenomenal, în special în ultimele două decenii. Vă mai aduceți aminte de monitoarele CRT (cele cu tub)? Cât de glorioase erau ele la o greutate considerabilă și culori anoste? Cu atâtea modele noi în fiecare lună, este ușor să uităm cât timp a trecut de la primele monitoare, este ușor să uităm ce am avut nu demult.
            Piața de monitoare a produs și a inovat continuu produsele sale pentru a se potrivi din ce în ce mai bine diferitelor nevoi și stiluri de viață ale clienților..</p>
        <div class="img">
            <img src="../images/CRT-LED.png" alt="CRT LED" width="700">
        </div>
        <br>
    </div>
    <hr>
</div>

<div class="footer">

    <?php include "../templates/footer.php"; ?>

</div>

</body>
</html>
