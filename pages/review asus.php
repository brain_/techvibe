<!DOCTYPE html>
<html lang="en">

<?php include "../templates/header.php"; ?>

<body>

<div class="main">
    <div class="centerbox">
        <h1>Review Router Asus RT-N66U</h1>
        <h5>Date added Ian 21, 2018</h5>
        <div class="img">
            <img src="../images/router_asus_rtn66u.png" alt="Router asus">
        </div>
        <p>Transmisie Dual-Band de 2,4 GHz și 5 GHz pentru putere mare de semnal și conexiunea ultra-rapidă de până la 900Mbps, cel mai bun router pentru acasă și pentru la muncă.</p>
        <p>Te joci mult sau petreci mult timp făcând streaming video? Indiferent de plan, RT-N66U deține tot ce ai nevoie. Făcut pentru cei care au o conexiune Gigabit și doresc să se utilizeze de aceasta la potențial maxim, acest router vine încărcat doar cu funcții utile și cu o performanță pe măsura prețului.</p>
        <p>Specificaţiile routerului Asus RT-N66U sunt următoarele:</p>
        <div class="list">
            <ul style="list-style-type:disc">
                <li>wireless 802.11a/b/g/n;</li>
                <li>transmisie simultană pe 2,4 GHz şi 5 GHz pentru 450 + 450 Mbps;</li>
                <li>3 antene externe detaşabile;</li>
                <li>4 porturi 10/100/1000 RJ-45 pentru reţea locală, plus un alt port gigabit pentru conexiunea la internet;</li>
                <li>două porturi USB 2.0 care dispun de suport pentru unităţi de stocare externe, imprimante şi modem-uri 3G;</li>
                <li>dimensiuni 207 x 148.8 x 35.5 mm.</li>
            </ul>
        </div>
        <p>Din punct de vedere al design-ului partea superioară a acestuia, spatele și antenele sunt facute dintr-un plastic mat, iar partea centrală a acestuia este dintr-un plastic texturizat cu dungi oblice.</p>
        <p>Cu un design versatil, RT-N66U poate fi prins pe un perete și încape aproape oriunde pe biroul de acasă sau de la serviciu, datorită unui suport care se aplică pe spatele acestuia, suport care îl face să stea în poziție aproape verticală.</p>
        <div class="img">
            <img src="../images/asus_suport.png" alt="Asus suport">
        </div>
        <p>După câteva ore de funcționare am observat că acesta s-a încalzit în zona frontală puțin mai mult decât mă așteptam față de modelele precedente.  Routerul beneficiază de un heat spreader mare care disipează caldura produsă de un procesor care rulează la 600Mhz, acesta ocupându-se de transmisia simultană pe 2.4Ghz și 5Ghz.</p>
        <div class="img">
            <img src="../images/asus-rt-ac66u.jpg" alt="Asus RT AC66U">
        </div>
        <p>Chiar și așa, după 8 luni de utilizare acesta nu a cauzat nici un fel de problemă, însă pe internet am văzut că ar fi persoane care montează ventilatoare de 92mm în partea frontală a routerului pentru o răcire mai bună, puțin exagerat și fără rost ținand cont ca router se afla pe piață din 2013 și nu a existat nici un fel de incident cu el legat de încălzirea acestuia.</p>
        <p>Interfața, ASUSWRT Dashboard UI a acestuia poate fi accesată pentru accesarea funcționalității suplimentare prin introducerea următoarei adrese în browserul preferat: http://192.168.1.1. Acesta continua să dispună de update-uri de firmare, ultimul fiind facut 31.03.2017</p>
        <p>ASUS Ai Radar oferă o putere mai mare dispozitivelor wireless într-un mod inteligent. Amplificatorul de înalta putere trimite semnalul optim în toate direcțiile pentru o acoperire mai bună și transmisie îmbunătățita de date.</p>
        <p>Una din funcția foarte utilă a routerului este Download Master care te lasă să descarci și să rulezi fișiere multimedia stocate pe un dispozitiv USB conectat la router, compatibil cu alte dispozitive compatibile PC, tableta, PS4 sau XBOX sau televizor cu internet. Download Master mai poate să descarce fișiere când PC-ul este închis și te lasă să monitorizezi statutul download-urilor.</p>
        <div class="img">
            <img src="../images/asus_router_backend.png" alt="Asus router backend">
        </div>
        <p>Trecem totuși la partea care ne intereseaza pe toți, cum se comportă el pe internet și cât de repede pot descărca ceva cu el.</p>
        <p>Mai jos puteți vedea rezultatele atât de pe un desktop echipat cu un procesor quad core (intel i5 6400) și un SSD (Intel 540s Series), cât și de pe un smartphone (Nexus 5X). Țin să menționez că toate testele au fost făcute pe o conexiune prin fibra optică oferită de cei de la RDS cu un pachet Digi Net Fiberlink 1000.</p>
        <div class="img">
            <img src="../images/speedtest_ct.png" alt="Speedtest ct">
            <img src="../images/speedtest_uk.png" alt="Speedtest uk">
        </div>
    </div>
</div>

<div class="footer">
    <?php include "../templates/footer.php"; ?>
</div>

</body>
</html>
