<!DOCTYPE html>
<html lang="en">

<?php include "../templates/header.php"; ?>

<body>

<div class="main">
    <div class="centerbox">
        <h1>ASUS Republic of Gamers anunță Strix GL12CX</h1>
        <h5>Date added Ian 21, 2018</h5>
        <div class="img">
            <img src="../images/rog_strix_gl12cx_3d.jpg" alt="Asus ROG">
        </div>
        <br>
        <p>ASUS Republic of Gamers (ROG) anunță Strix GL12CX, un desktop de gaming supratactat din fabrică, ce dispune de cel mai nou procesor Intel Core i9-9900K, cu 8 nuclee, din generația a noua și grafica NVIDIA GeForce RTX. Procesorul Intel Core din generația a noua este deblocat pentru ajustări precise cu ajutorul software-ului ROG Armoury Crate, ce permite printre altele și personalizarea iluminatului Aura Sync din interior și exterior. În cadrul sertarului SSD hot-swap gândit pentru competițiile de gaming există un modul DIMM.2 care poate conecta două SSD-uri răcite prin radiatoare dedicate. Având componente de ultimă oră și conectivitate extinsă într-un design ușor accesibil, Strix GL12CX oferă astăzi o experiență de gaming uimitoare, cu posibilitatea de dezvoltare pe viitor.</p>
        <br>
        <p>Strix GL12CX este construit pe baza celor mai puternice componente disponibile, începând cu cel mai bun procesor pentru gaming: noul Intel Core i9-9900K din generația a noua. Având opt nuclee și 16 fire de execuție, noul procesor Intel este capabil să ofere un gaming rapid, în timp ce rulează simultan fluxuri de streaming, înregistrarea acțiunii și jonglează cu chat-ul și alte aplicații. Acesta este ideal pentru profesioniștii din domeniul creației, oferind performanțe incredibile, în activități cu fluxuri multiple cum ar fi randarea, transcodarea, precum și alte sarcini.</p>
        <br>
        <p>“Suntem încântați să lucrăm cu ASUS la lansarea ROG Strix GL12CX, oferindu-le entuziaștilor performanțele de top în gaming și capacitățile unei platforme robuste, furnizate de procesorul Intel Core i9 din generația a noua împreună cu acest design uimitor“, a declarat Anand Srivatsa, vicepreședinte și manager general Desktop, Systems and Channel al Client Computing Group din cadrul Intel. Vivian Lien, șeful Global Marketing for Gaming în cadrul Republic of Gamers și Chief Marketing Officer în cadrul ASUS North America a adăugat: “ASUS este încântat să construiască pe baza relației strânse pe care o are cu Intel pentru a furniza deopotrivă jucătorilor și profesioniștilor din sportul electronic cea mai bună performanță. ROG Strix GL12CX este făcut să domine cele mai noi jocuri cu procesorul său cu opt nuclee, supratactat, răcit cu lichid, și dispune de asemenea, de primul sistem din industrie ce integrează iluminatului Aura Gaming cu jocurile compatibile, semnalizând schimbările de mediu și alte indicii vizuale pentru îmbunătățirea experienței de joc”.</p>
        <div class="img">
            <img src="../images/rog_strix_gl12cx_3d_front.jpg" alt="Asus ROG front">
        </div>
        <p>Strix GL12CX este conceput cu o conectivitate extinsă pentru a suporta toate perifericele și cerințele utilizatorului. Alături de sertarul frontal SSD hot-swap, un alt panou frontal include un cititor de carduri 2-in-1, două porturi USB 3.1 și două porturi USB 2.0. Strix GL12CX dispune, de asemenea, de o unitate optică pentru DVD-urile vechi. Pe spate sunt disponibile șase porturi USB alături de cinci conectori audio și un conector S/PDIF pentru receptoarele compatibile. Placa Wi-Fi 802.11ac Wave 2 este capabilă de viteze mai mari decât cele gigabit și include, de asemenea, Bluetooth 5.0 pentru a conecta cele mai recente periferice în modurile cu emisie redusă de energie, conectând mai multe dispozitive simultan și mărind totodată distanța și rata de transfer.</p>
        <div class="list">
            <ul style="list-style-type:disc">
                <li>Performanțe de ultimă oră: Procesor Intel Core i9-9900K, 32GB RAM, grafică GeForce RTX 2080 Ti 11GB și SSD 512GB PCIe 3.0 x4</li>
                <li>Sistem de răcire de top pentru procesoarele selectate: Procesoare supratactate din fabrică și răcire cu lichid pentru a susține frecvențe de 4.9GHz pe toate nucleele și firele de execuție</li>
                <li>Stocare inovatoare: Sertarul SSD hot-swap face din GL12CX un star al sportului electronic, modulul inovator DIMM.2 oferă stocare flexibilă, fără precedent</li>
                <li>Iluminat personalizat: GL12CX și perifericele compatibile Aura Sync pot reacționa la evenimentele din joc, în titlurile compatibile, cum ar fi Call of Duty: Black Ops 4</li>
                <li>Conectivitate extinsă: Conectivitate cuprinzătoare incluzând Wi-Fi 802.11ac din clasa gigabit, Bluetooth 5.0, porturi USB 3.1/2.0 și cititor de carduri 2-in-1.</li>
            </ul>
        </div>
        <div class="img">
            <img src="../images/rog_strix_gl12cx_side.jpg" alt="Asus ROG side">
        </div>
        <p>Cum ai putea testa un astfel de sistem dacă nu cu unul dintre premiile concursului nostru, Jocuri Gratis?</p>
    </div>
</div>
</div>

<div class="footer">
    <?php include "../templates/footer.php"; ?>
</div>

</body>
</html>
