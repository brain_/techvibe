<!DOCTYPE html>
<html lang="en">

<?php include "../templates/header.php"; ?>

<body>

<div class="main">
    <div class="centerbox">
        <h1>ASUS Republic of Gamers lansează placa video ROG Strix GeForce RTX 2080 Ti OC Call of Duty: Black Ops 4 Edition</h1>
        <h5>Date added Ian 23, 2018</h5>
        <div class="img">
            <img src="../images/ROG-Strix-GeForce-RTX-2080-Ti-OC-3.jpg" alt="Asus ROG geforce 3" width="710">
        </div>
        <p><b>ROG Strix GeForce RTX 2080 Ti OC</b></p>
        <div class="list"><b>
                <ul style="list-style-type:disc">
                    <li>ROG Strix GeForce RTX 2080 Ti OC Call of Duty: Black Ops 4 Edition dispune de o emblemă iluminată și include ediția digitală a jocului pentru PC.</li>
                    <li>Acceleratorul are un design extins pe 2,7 sloturi pentru a maximiza suprafața de răcire, beneficiind de standardul IP5X pentru rezistența la praf. Ventilatoarele axiale îmbunătățesc fluxul de aer.</li>
                    <li>Integrare Aura Sync cu Call of Duty: Black Ops 4 pentru acțiunea din joc și o experiență îmbunătățită.</li>
                    <li>Noua ediție va fi disponilă și în România la sfârșitul lunii decembrie 2018.</li>
                </ul>
            </b>
        </div>
        <p>ASUS Republic of Gamers (ROG) a lansat placa video ROG Strix GeForce RTX 2080 Ti OC Call of Duty: Black Ops 4 Edition. Această ediție va fi disponibilă în cantități limitate, fiind restricționată la doar 500 de unități disponibile numai în anumite zone geografice (inclusiv în România). Decorate cu insigna Call of Duty: Black Ops 4 Edition, plăcile împerechează nuclee NVIDIA din lista A cu performanțe de vârf și o răcire extremă. Acestea au frecvențe de până la 1665MHz, performanțele rămânând constante în timpul acțiunilor intensive.</p>
        <div class="img">
            <img src="../images/ROG-Strix-GeForce-RTX-2080-Ti-OC-1.jpg" alt="Asus ROG strix geforce 1">
        </div>
        <p><b>VRM cu 16 faze</b>
            <br>Din moment ce frecvența de ceas a plăcii este suficientă pentru a genera rate de cadre ridicate la rezoluții extreme, jucătorii vor dori să stoarcă până și ultima picătură de performanță de la arhitectura Turing. Armată cu bobine Super Alloy, condensatori din polimeri solizi și cu o matrice cu 16 etaje de alimentare, ROG Strix GeForce RTX 2080 Ti OC Call of Duty: Black Ops 4 Edition a fost concepută să împingă la limite nucleul grafic.</p>
        <p><b>Tehnologia Auto-Extreme</b>
            <br>Toate plăcile grafice ROG Strix sunt produse prin utilizarea tehnologiei Auto-Extreme, un proces automat de fabricație ce stabilește noi standarde în industrie prin executarea lipiturilor într-un singur pas. Acesta reduce stresul termic asupra componentelor, reduce consumul de energie în procesul de fabricație și îmbunătățește fiabilitatea generală.</p>
        <p><b>Integrare Aura Sync </b>
            <br>ROG a colaborat cu Activision în cazul jocului Call of Duty: Black Ops 4 pentru ca efectele din joc să fie exprimate prin sistemul de iluminat Aura. Logoul din partea din spate a plăcii grafice pulsează sincronizat cu cronometrarea rundelor și își schimbă culoarea atunci când personajul este sub apă sau când jocul trece în modul Blackout. Software-ul Aura Sync permite ca iluminarea să fie coordonată cu alte elemente hardware inspirate de ROG Call of Duty: Black Ops 4 și accesorii compatibile Aura, astfel că efectele de imersie pot fi transmise de sistem în tot desktopul.</p>
        <div class="img">
            <img src="../images/ROG-Strix-GeForce-RTX-2080-Ti-OC-2.jpg" alt="Asus ROG strix geforce 2">
        </div>
        <p><b>Disponibilitate</b>
            <br>Placa video ROG Strix GeForce RTX 2080 Ti OC Call of Duty: Black Ops 4 Edition va fi disponibilă în România la finalul lunii decembrie 2018, prin intermediul magazinelor partenere.</p>
    </div>
</div>
</div>

<div class="footer">
    <?php include "../templates/footer.php"; ?>
</div>

</body>
</html>
