<!DOCTYPE html>
<html lang="en">

<?php include "../templates/header.php"; ?>

<body>

<div class="main">
    <div class="centerbox">
        <h1>Cum a fost la Bucharest Gaming Week</h1>
        <h5>Date added Ian 17, 2018</h5>
        <div class="img">
            <img src="../images/image007.jpg" alt="bucharest gaming week" width="900">
        </div>
        <p><b>Bucharest Gaming Week</b> a strâns la prima ediție peste 10.000 de pasionați de esports, gameri dedicați și oameni din industria de game development din România, majoritatea participanților fiind prezentă în weekendul 27-28 ianuarie. Vizitatorii au participat la numeroase activități care s-au desfășurat la standurile expozanților unde au avut loc peste 50 de competiții și tombole, iar fani de toate vârstele s-au adunat pentru a se întâlni cu creatorii de conținut, jucătorii profesioniști și youtuberii preferați.</p>
        <div class="img">
            <img src="../images/BGW_4.jpg" alt="VR" width="900">
        </div>
        <p>Evenimentul central a avut loc la Romexpo, unde atracțiile principale au fost finalele esport ESL Southeast Europe Championship și Finala Sezonului 3 al Turneului Național de League of Legends. Echipele câștigătoare au fost  BPro (Bulgaria) la turneul de Counter Strike: Global Offensive, echipa românească Nexus Gaming la League of Legends și jucătorul Georgios “Likeabawse” Papadimitrakopoulos (Grecia) la turneul de Hearthstone. Finala Sezonului 3 a Turneului Național de League of Legends a fost câștigată de echipa românească RedFear. Premiile totale ale acestor competiții au depășit suma de 16.000 Euro.</p>
        <div class="img">
            <img src="../images/BGW_1.jpg" alt="BGW1" width="900">
        </div>
        <p>În cadrul Bucharest Gaming Week vizitatorii au participat la activități diverse: un workshop de game design dedicat liceenilor, o expoziție de concept art cu creații din jocurile realizate în România, un panel cu reprezentanți ai industriei de game development din țară și un game jam de 48 de ore care s-a concretizat cu un număr de 15 jocuri digitale și non-digitale realizate în timp record.</p>
        <div class="img">
            <img src="../images/BGW_6.jpg" alt="BGW6" width="900">
        </div>
        <p>Printre momentele de top ale evenimentului central de la Romexpo se enumeră prezența Armatei României, turneul de PlayerUnknown’s Battlegrounds Romania Invitational susținut de HyperX, Turneul de Clash Royale, cu premiu de 1000 de euro, turneul Gateway VR Arena, organizat de către cei de la Gateway VR și prezența studiourilor românești de game development, în cadrul standului RGDA.</p>
        <div class="img">
            <img src="../images/BGW_7.jpg" alt="BGW7" width="900">
        </div>
    </div>
</div>

<div class="footer">
    <?php include "../templates/footer.php"; ?>
</div>

</body>
</html>
