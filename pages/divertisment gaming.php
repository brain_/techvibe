<!DOCTYPE html>
<html lang="en">

<?php include "../templates/header.php"; ?>

<body>

<div class="main">
    <div class="centerbox">
        <h1>Gaming-ul are public mai mare decât HBO, Hulu, Neftlix și ESPN la un loc</h1>
        <h5>Date added Ian 14, 2018</h5>
        <div class="img">
            <img src="../images/1_26BIgJm5GZCdYO31v2B1lg.png" alt="Gaming" width="900">
        </div>
        <p>Așa arată un nou raport al SuperData Research, ce conclude că ne place gaming-ul foarte mult.</p>
        <p>Conform SuperData, întreaga audiență dedicată gaming-ului include peste 665 de milioane de oameni și crește de la zi la zi: compania prezice o creștere de 21% până în 2021.</p>
        <p>Aceștia susțin că audiența pentru industria de gaming depășește audiența totală a HBO, Netflix, ESPN și Hulu. Drept exemplu, numărul abonaților Netflix este de aproape 100 de milioane, în timp ce Hulu are aproximativ 12 milioane. Comparați aceste date cu baza de subscriberi a lui PewDiePie, singur, care are peste 54 de milioane de subscriberi pe YouTube.</p>
        <p>Totuși, nu e de mirat, având în vedere că Netflix nu este disponibil în China, unde populația este atât de mare. Cu toate acestea, cu atâtea servicii de streaming pentru gaming, industria va crește de la zi la zi.</p>
        <p>Din totalul celor care urmăresc content dedicat gaming-ului, 54% sunt bărbați și 46% femei. Per total, această creștere nu poate să însemne decât bine pentru industria gaming-ului, având în vedere că cei care urmăresc astfel de content sunt mai dispuși să cheltuie bani pentru jocuri.</p>
        <p><b>Alte cifre interesante:</b></p>
        <p>League Of Legends a fost urmărit peste 100 de milioane de ore numai în februarie, iar CS:GO aproape 40 de milioane.</p>
        <p>Contentul de gaming este cel mai important și masiv de la social media încoace. La o audiență totală de peste 660 milioane de oameni, brandurile și advertiserii sigur vor profita de asta. O putem lua ca pe un lucru pozitiv, doar crește industria de care suntem pasionați, nu? Să sperăm că gaming-ul va menține o direcție bună.</p>
        <p>Reclamele și cheltuielile din gaming au ajuns la aproape 3.2 miliarde de dolari la sfârșitul anului trecut.</p>
        <p>Contentul video este un musai pentru publisheri, având în vedere că 2 din 3 persoane din public urmăresc așa ceva pentru a învăța mai mult despre jocul lor preferat.</p>
    </div>
</div>

<div class="footer">
    <?php include "../templates/footer.php"; ?>
</div>

</body>
</html>
