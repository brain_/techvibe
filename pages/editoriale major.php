<!DOCTYPE html>
<html lang="en">

<?php include "../templates/header.php"; ?>

<body>

<div class="main">
    <div class="centerbox">
        <h1>Ce inseamna un Major</h1>
        <h5>Date added Ian 18, 2018</h5>
        <p><b>Dota 2 este precum un uragan: într-o continuă crestere şi dezvoltare. Deşi alte sporturi electronice vin şi pleacă, opera celor de la Valve se modelează, evoluează şi reprezintă un pilon de susţinere  pentru viitorul esports.</p></b>
        <div class="img">
            <img src="../images/PGLgroup.jpg" alt="PGL Group"" width="900">
        </div>
        <p>Creatorii jocului au început să pună bazele unui turneu de talie mondială încă din 2011, dând startul la The International. Odată cu acest turneu s-a produs o reacţie chimică spectaculoasă, iar privirile celor mai mari sponsori şi organizaţii de esports din lume s-au întors cu privirile spre Dota 2. Astfel, din 2011, organizatorii de la nivel mondial aveau mână liberă să sustină competiţii de Dota 2. În ciuda numeroaselor turnee organizate, Valve s-a implicat anual strict in organizarea The International.</p>
        <div class="img">
            <img src="../images/kiev%20major.jpg" alt="Kiev Major"" width="900">
        </div>
        <p>În aprilie 2015, echipa lui IceFrog a pus la cale cea mai mare şi semnificativă schimbare pe scena profesionistă: introducerea în circuit a turneelor de tip Major şi Minors. Acestea au ca şi scop implementarea unei noi tradiţii: o comeptiţie la nivel mondial pentru toamnă, iarna, primavară şi vară (The International). Majors au fost catalogate ca şi importanţă drept cele mai ample şi impactante turnee din scena competitivă. Cu atât mai mult, acestea au devenit de neocolit pentru jucătorii profesionişti odată cu introducerea punctelor de calificare care, cumulate, creşte şansa echipelor de a participa la The International. Nici premiile oferite de către Valve la aceste evenimente nu sunt de lepădat: de la câteva mii de dolari la cateva milioane, sau zeci de milioane.</p>
        <div class="img">
            <img src="../images/dota%20championship.jpg" alt="Dota Championship"" width="900">
        </div>
        <p>În vara anului 2017, Valve anunţă programul preliminar al turneelor pentru anul 2017 şi 2018, constând din 11 Majors şi 16 Minors, echipele câştigătoare la Majors primind un număr impresionant de 1500 de puncte de calificare.</p>
        <p>Organizatorii de la Bucharest Major, PGL, deţin deja un record de turnee de tip major organizate: Manila şi Boston 2017, precum şi Kiev 2017, având parte de o audienţă pe măsură. Toţi fanii Dota 2 din lume vor privi cu entuziasm şi nerăbdare turneul ce va avea loc în ţara noastră. Etapa calificărilor a început deja şi poate fi urmărită pe twitch-ul lor oficial.</p>
    </div>
</div>

<div class="footer">
    <?php include "../templates/footer.php"; ?>
</div>

</body>
</html>
