<!DOCTYPE html>
<html lang="en">

<?php include "../templates/header.php"; ?>

<body>

<div class="main">
    <div class="centerbox">
        <h1>De la CRT la LED: O călătorie prin timp</h1>
        <h5>Date added Ian 15, 2018</h5>
        <div class="img">
            <img src="../images/CRT-LED.png" alt="CRT LED" width="900">
        </div>
        <h2>Campionul necontestat timp de 50 de ani, CRT-ul</h2>
        <p>De la CRT la LED: Industria display-urilor a avansat într-un ritm fenomenal, în special în ultimele două decenii. Vă mai aduceți aminte de monitoarele CRT (cele cu tub)? Cât de glorioase erau ele la o greutate considerabilă și culori anoste? Cu atâtea modele noi în fiecare lună, este ușor să uităm cât timp a trecut de la primele monitoare, este ușor să uităm ce am avut nu demult.
            Piața de monitoare a produs și a inovat continuu produsele sale pentru a se potrivi din ce în ce mai bine diferitelor nevoi și stiluri de viață ale clienților.</p>
        <p>Dar între atâtea modele noi, rezoluții 4K, monitoare curbate, cine își mai aduce aminte cum am ajuns aici?</p>
        <p>Dacă ne întoarcem în anii 90 și ne propunem un set-up de 2-3 monitoare ar trebui întâi de toate să ne construim o masă de beton, doar ca să susțină greutatea acestora. Mai departe, pe parte de utilizare, singurul motiv pentru care am face asta este să încercăm să fim mai productivi, fiindcă de jocuri cu suport eye-finity nu s-ar pune problema, la ce rame groase erau atunci.</p>
        <p>Deși erau foarte avansate în acea perioadă din punct de vedere al calității imaginii și al rezoluției, puțini știu faptul că primul monitor CRT datează din 1897, dezvoltat de către Karl Ferdinand Braun, un fizician german.>/
        <p>Datorită creșterii popularității esports și a jocurilor competitive, utilizatorii din ziua de astăzi au nevoie de ecrane care nu oferă doar timp scurt de răspuns și o rată mare de refresh, dar și cu design care permite o experiență de joc captivantă. De-a lungul întregii istoriei monitoarelor, ecranele curbate – care, în mod surprinzător, își făcuseră debutul în sectorul televiziunii în 1952 – au devenit populare printre comunitățile de gameri.</p>
        <p>Opțiunile de ajustare ergonomică au evoluat și ele de-a lungul timpului. Monitoarele CRT rareori au depășit funcții de bază de pivotare, în timp ce multe dintre ecranele de astăzi pot fi înclinate, rotire și ajustate în înălțime.</p>
    </div>
</div>

<div class="footer">
    <?php include "../templates/footer.php"; ?>
</div>

</body>
</html>
