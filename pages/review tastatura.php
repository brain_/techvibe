<!DOCTYPE html>
<html lang="en">

<?php include "../templates/header.php"; ?>

<body>

<div class="main">
    <div class="centerbox">
        <h1>Review Tastatură Logitech G105</h1>
        <h5>Date added Ian 19, 2018</h5>
        <div class="img">
            <img src="../images/logitech_g105.png" alt="Logitech G105" width="900">
        </div>
        <p>Logitech Gaming Keyboard G105 este o tastatură pe membrană silențioasă care ofera foarte multe optiuni. În ciuda faptului că aceasta nu este o tastatură mecanică, aceasta oferă o functionalitate pentru orice tip de utilizator, design atractiv si un pret relativ accesibil.</p>
        <p>Tastatura are șase taste G programabile, fiecare cu trei moduri de stare, oferă pâna la 18 combinații de macro-uri. Macro-urile pot fi realizate prin apasarea unei singure taste sau prin combinații complexe de mai multe taste. Aceasta vine cu funcția key rollover îmbunătățită și funcția anti-ghosting-ul înseamnă că poti duce mai multe acțiuni, mai complexe, fără interferențele sau efectul de ghosting care pot fi gasite pe alte tastaturi. Toate aceste funcții pot fi configurate din Logitech Gaming Software, care se poate descărca de pe site-ul producătorului.</p>
        <p>Iluminarea LED poate fi setată pe două nivele de luminozitate. În întuneric, tastele sunt destul de vizibile întru cât să permită utilizarea ei pe timp de noapte fară nici un fel de problemă. Cele doua nivele de luminozitate pot fi controlate cu butonul din partea stângă aflată în partea de sus a tastaturii și nu necesită instalarea unui software adițional.</p>
        <p>Ținând cont că Logitech are pe piață un număr considerabil de tasturi mecanice, aceasta cade în umbra acelor produse, căci de foarte multe ori G105 nu se ridică la nivel așteptărilor sau a prețului pentru mulți dintre potențialii săi utilizatori. Aceasta poate fi gasită în magazine la 240 lei (în momentul scrierii acestui review) și nu poate fi considerat un preț enorm pentru funcționalitatea pe care o oferă aceasta, dar poate fi considerat ceva mai mare decât restul produselor oferite de concurență.</p>
        <p>Singurele două probleme pe care le-am putut găsi cu acest model este că uneori macro-urile nu funcționează toate corespunzator, dar este o problema întălnită rar, iar a doua ar fi ca tasta Shift din partea stangă este destul scurtă, iar pentru anumiți utilizatori ar putea creea neplăceri până se obișnuiesc cu layout-ul acesteia.</p>
        <p>În concluzie, aceasta nu este o tastatură proastă, ci una pentru cei care nu doresc sau nu vor sa faca tranziția pe o tastatură mecanică.</p>
    </div>
</div>

<div class="footer">
    <?php include "../templates/footer.php"; ?>
</div>

</body>
</html>
